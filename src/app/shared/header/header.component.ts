import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MatDialog } from '@angular/material';

import { UserModel } from '../models/users/user.model';
import { AccountService } from '../services/account.service';
import { CartDialogComponent } from 'src/app/cart/cart-dialog/cart-dialog.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  entryComponents:[CartDialogComponent]
})

export class HeaderComponent implements OnInit {

  currentUser: UserModel;
  test: string;
  
  constructor(
    private router: Router,
    private accountService: AccountService,
    private dialog: MatDialog,
  ) {
    this.accountService.currentUser.subscribe(x => this.currentUser = x);
  }
  ngOnInit() {

  }

  get isAdmin() {
    return this.currentUser && this.currentUser.role === "admin";
  }

  get isUser() {
    return this.currentUser && this.currentUser.role === "user";
  }

  showCart() {
    this.dialog.open(CartDialogComponent,{
      width: '600px',
    });
  }

  logout() {
    this.accountService.logout();
    this.router.navigate(['account/login']);
  }
}