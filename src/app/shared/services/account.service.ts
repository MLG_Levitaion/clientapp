import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

import { UserModel } from '../models/users/user.model';
import { RegisterModel } from '../models/account/register.model';
import { ChangePasswordModel } from '../models/account';
import { BaseModel } from '../models/base';

@Injectable({ providedIn: 'root' })

export class AccountService {

    private currentUserSubject: BehaviorSubject<UserModel>;
    public currentUser: Observable<UserModel>;
    private url = "http://localhost:5001/api/account/";

    httpClient: any;

    constructor(
        private http: HttpClient,
        private cookieService: CookieService) {
        this.currentUserSubject = new BehaviorSubject<UserModel>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): UserModel {
        return this.currentUserSubject.value;
    }
    setUserValue(userModel: UserModel) {
        localStorage.setItem('currentUser', JSON.stringify(userModel));
    }

    login(email: string, password: string): Observable<UserModel> {
        return this.http.post<UserModel>(this.url + "login", { email, password }, { withCredentials: true })
            .pipe(map(UserModel => {
                if (UserModel.id !== 0) {
                    localStorage.setItem('currentUser', JSON.stringify(UserModel));
                    this.currentUserSubject.next(UserModel);
                    return UserModel;
                }
                return UserModel;
            }));
    }

    refreshTokens(accessToken: string, refreshToken: string): Observable<any> {
        return this.http.post<any>(this.url + "refreshtokens", { accessToken, refreshToken })
    }

    logout() {
        this.cookieService.delete("AccessToken");
        this.cookieService.delete("RefreshToken");

        localStorage.removeItem('currentUser');
        localStorage.removeItem('order');
        
        this.currentUserSubject.next(null);
    }

    register(user: RegisterModel): Observable<BaseModel> {
        return this.http.post<BaseModel>(this.url + "register", user);
    }

    confirmemail(token: string): Observable<boolean> {
        return this.http.post<boolean>(this.url + "emailconfirm", { token });
    }

    forgotpassword(email: string): Observable<BaseModel> {
        return this.http.post<BaseModel>(this.url + "forgotpassword", { email });
    }

    delete(id: string) {
        return this.http.get(this.url + "test");
    }

    changePassword(changePasswordModel: ChangePasswordModel): Observable<BaseModel> {
        return this.http.post<BaseModel>(this.url + "changepassword", changePasswordModel);
    }
}