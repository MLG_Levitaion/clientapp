import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { UserModel } from '../models/users/user.model';
import { FilterUserModel } from '../models/users';
import { ResponseModel } from '../models/base';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url = "http://localhost:5001/api/user/";
  httpClient: any;

  constructor(private http: HttpClient) { }

  getFiltered(filterModel: FilterUserModel): Observable<ResponseModel<UserModel>> {
    return this.http.post<ResponseModel<UserModel>>(this.url + "getall", filterModel);
  }

  getCurrentUser(): Observable<UserModel> {
    return this.http.get<UserModel>(this.url + "getuser");
  }

  update(user: UserModel): Observable<UserModel> {
    return this.http.put<UserModel>(this.url + "update", user);
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(this.url + 'delete?id=' + id);
  }
}
