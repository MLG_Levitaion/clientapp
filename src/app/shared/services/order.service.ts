import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { FilterOrderModel, ResponseOrderModel, RequestOrderModel } from '../models/order';
import { ResponseModel, BaseModel } from '../models/base';
import { PaymentModel } from '../models/payment/payment.model';

@Injectable({
    providedIn: 'root'
})
export class OrderService {
    
    private url = "http://localhost:5001/api/order/";
    httpClient: any;

    constructor(private http: HttpClient) {
    }

    create(order: RequestOrderModel): Observable<ResponseOrderModel> {
        return this.http.post<ResponseOrderModel>(this.url + 'create', order);
    }

    getFiltered(filterModel: FilterOrderModel): Observable<ResponseModel<ResponseOrderModel>> {
        return this.http.post<ResponseModel<ResponseOrderModel>>(this.url + "getfiltered", filterModel);
    }

    get(orderId: number): Observable<ResponseOrderModel> {
        return this.http.get<ResponseOrderModel>(this.url + "get?id=" + orderId)
    }

    update(payment: PaymentModel): Observable<BaseModel> {
        return this.http.post<BaseModel>(this.url + 'pay', payment);
    }

    delete(id: number): Observable<void> {
        return this.http.delete<void>(this.url + 'delete?id=' + id);
    }
}