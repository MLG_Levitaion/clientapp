import { Injectable, Predicate } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class PaymentService {

  constructor(
  ) {

  }

  pay(totalPrice: number, callBackPredicate: Predicate<string>) {
    var handler = (<any>window).StripeCheckout.configure({
      key: 'pk_test_0u7IbRIcdouwZw1WiVkJg0DW00SIRSKhzj',
      locale: 'auto',
      token: function (token: any) {
        callBackPredicate(token.id);
      }
    });
    
    handler.open({
      name: 'Demo Site',
      description: '2 widgets',
      amount: totalPrice * 100
    });
  }
}

