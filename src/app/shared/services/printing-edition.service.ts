import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { FilterPrintingEditionModel } from '../models/printing-editions/filter-printing-edition.model';
import { PrintingEditionModel } from '../models/printing-editions';
import { ResponseModel } from '../models/base';
import { Currency } from '../models/enums';
import { ConvertPriceModel } from '../models/printing-editions/convert-price.model';

@Injectable({
  providedIn: 'root'
})

export class PrintingEditionService{

    private url = "http://localhost:5001/api/printingedition/";
    httpClient: any;
    constructor(private http: HttpClient) {
    }
  
    get(filtermodel: FilterPrintingEditionModel): Observable<ResponseModel<PrintingEditionModel>> {
      return this.http.post<ResponseModel<PrintingEditionModel>>(this.url + "getall", filtermodel);
    }
  
    create(printingedition: PrintingEditionModel): Observable<PrintingEditionModel> {
      return this.http.post<PrintingEditionModel>(this.url + "create", printingedition);
    }

    update(printingedition: PrintingEditionModel): Observable<PrintingEditionModel> {
      debugger;
      return this.http.put<PrintingEditionModel>(this.url + "update", printingedition);
    }

    delete(id: number):Observable<void> {
      return this.http.delete<void>(this.url + 'delete?id='+id);
    }
    
    convert(price: number, currency: Currency):Observable<ConvertPriceModel>{
      return this.http.post<ConvertPriceModel>(this.url + 'convert',{price,currency}); 
    }
}