import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { FilterAuthorModel } from '../models/authors/filter-author.model';
import { ResponseModel } from '../models/base/response.model';
import { ResponseAuthorModelItem } from '../models/authors/response-author.model';
import { AuthorModel } from '../models/authors/author.model';

@Injectable({
  providedIn: 'root'
})

export class AuthorService {

  private url = "http://localhost:5001/api/author/";
  httpClient: any;
  
  constructor(private http: HttpClient) {
  }

  getall() {
    return this.http.get<ResponseModel<ResponseAuthorModelItem>>(this.url + "getall");
  }

  getFiltered(filtermodel: FilterAuthorModel): Observable<ResponseModel<ResponseAuthorModelItem>> {
    return this.http.post<ResponseModel<ResponseAuthorModelItem>>(this.url + "getauthors", filtermodel);
  }

  create(author: AuthorModel): Observable<AuthorModel> {
    return this.http.post<AuthorModel>(this.url + "create", author);
  }
  update(author: AuthorModel): Observable<AuthorModel> {
    return this.http.put<AuthorModel>(this.url + "update", author);
  }
  delete(id: number): Observable<void> {
    return this.http.delete<void>(this.url + 'delete?id=' + id);
  }
}