import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AccountService } from '../services/account.service';


@Injectable({ providedIn: 'root' })

export class UserGuard implements CanActivate {
    constructor(
        private router: Router,
        private accountService: AccountService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.accountService.currentUserValue;
        
        if (currentUser.role == "user") {
            return true;
        }

        if(currentUser.role =="admin"){
            this.router.navigate(['/list']);
            return false;
        }
         
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}