export class ChangePasswordModel{
    id: number;
    newPassword: string;
    oldPassword: string;
}