import { FilterBaseModel } from '../base/filter-base.model';

export class FilterUserModel extends FilterBaseModel {
    isBlocked: boolean;
}