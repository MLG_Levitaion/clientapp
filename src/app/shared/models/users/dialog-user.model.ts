import { UserModel } from './user.model';

export class UserDialogModel extends UserModel{
    title: string;
    okButtonText: string;
}