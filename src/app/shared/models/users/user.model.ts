import { BaseModel } from '../base/base.model';

export class UserModel extends BaseModel{
    id: number;
    profilePicture: string;
    firstName: string;
    lastName: string;
    userName: string;
    email: string;
    role: string;
    isBlocked: boolean;
}