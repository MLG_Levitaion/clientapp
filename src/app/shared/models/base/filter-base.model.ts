import { SortBy } from '../enums/sort-by.enum';


export class FilterBaseModel{
    searchString: string;
    page: number;
    pageSize: number;
    sortField: string;
    sortBy: SortBy;
}