import { BaseModel } from '../base';
import { ResponseOrderItemModel } from '../order-item/response-order-item.model';

export class ResponseOrderModel extends BaseModel{
    id: number;
    creationDate: string;
    userName: string;
    userEmail: string;
    orderAmount: number;
    status: string;
    orderItems: ResponseOrderItemModel[];
}