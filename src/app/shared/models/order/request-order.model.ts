import { BaseModel } from '../base';
import { ReqestOrderItemModel } from '../order-item/request-ored-item.model';

export class RequestOrderModel extends BaseModel{
    id: number;
    description: string;
    userId: number;
    orderItems: ReqestOrderItemModel[];
}