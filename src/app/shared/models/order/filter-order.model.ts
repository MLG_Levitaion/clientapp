import { FilterBaseModel } from '../base';
import { OrderStatus } from '../enums';

export class FilterOrderModel extends FilterBaseModel{
    status: OrderStatus[];
    idUser: number;
}