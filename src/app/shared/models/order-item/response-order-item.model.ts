import { BaseModel } from '../base';

export class ResponseOrderItemModel extends BaseModel{
    title: string;
    productType: string;
    count: number;
    amount: number;
}