import { BaseModel } from '../base';

export class ReqestOrderItemModel extends BaseModel{
    printingEditionId: number;
    count: number;
    price: number;
    amount: number;
    title: string;
}