import { BaseModel } from '../base/base.model';

export class AuthorModel extends BaseModel{    
    id: number;
    name: string;
}