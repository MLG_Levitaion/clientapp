import { AuthorModel } from './author.model';

export class ResponseAuthorModelItem extends AuthorModel{
    printingEditions: string[];
}