import { AuthorModel } from './author.model';

export class AuthorDialogModel extends AuthorModel{
    title: string;
    okButtonText: string;
}