export enum OrderStatus {
    none = 0,
    unpaid = 1,
    paid = 2
}