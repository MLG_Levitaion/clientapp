export enum SortBy{
    none = 0,
    ascending = 1,
    descending = 2,
}