import { Currency } from '../enums';

export class ConvertPriceModel{
    price: number;
    currency : Currency;
}