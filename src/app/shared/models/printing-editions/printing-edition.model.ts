import { BaseModel } from '../base';
import { AuthorModel } from '../authors';

export class PrintingEditionModel extends BaseModel{
    id: number;
    title: string;
    description: string;
    price: number;
    currency: string;
    productType: string;
    authors: AuthorModel[];
}