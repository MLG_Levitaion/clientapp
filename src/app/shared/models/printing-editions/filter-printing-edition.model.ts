import { FilterBaseModel } from '../base';
import { ProductType, Currency } from '../enums';

export class FilterPrintingEditionModel extends FilterBaseModel{
    minPrice: number;
    maxPrice: number;
    printingEditionTypes: ProductType[];
    currency: Currency;
}