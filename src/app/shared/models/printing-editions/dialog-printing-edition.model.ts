import { PrintingEditionModel } from './printing-edition.model';

export class PrintingEditionDialogModel extends PrintingEditionModel{
    dialogtitle: string;
    okButtonText: string;
}