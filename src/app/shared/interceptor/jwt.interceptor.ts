import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(
        private cookieService : CookieService
        ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
       
        let accessToken = this.cookieService.get("AccessToken");
        let currentUser = localStorage.getItem('currentUser');

        if (accessToken && currentUser) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${accessToken}`                    
                }
            });
        }
        
        return next.handle(request);
    }
}