import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AccountService } from '../services/account.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    
    constructor(
        private accountService: AccountService,
        private cookieService: CookieService,
        private router: Router
        ) {
            
         }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {

                let accessToken = this.cookieService.get("AccessToken");
                let refreshToken = this.cookieService.get("RefreshToken");

                if (!accessToken && !refreshToken) {
                    this.accountService.logout();
                    this.router.navigate(['/account/login']);
                }

                this.cookieService.delete("AccessToken");
                this.cookieService.delete("RefreshToken");

                this.accountService.refreshTokens(accessToken, refreshToken).subscribe(data => {
                    if (accessToken && refreshToken) {
                        this.cookieService.set("AccessToken", data.accessToken);
                        this.cookieService.set("RefreshToken", data.refreshToken);
                    }
                    location.reload();
                });
            }

            if(err.status === 403){
                this.router.navigate(['/']);
            }

            const error = err.error.message || err.statusText;
            return throwError(error);
        }))
    }
}