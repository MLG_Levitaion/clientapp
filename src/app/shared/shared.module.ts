import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { SharedRoutingModule } from './shared-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MaterialModule } from './material/material.module';
import { CartModule } from '../cart/cart.module';


@NgModule({
  imports: [    
    CartModule,
    BrowserModule, 
    CommonModule,
    FormsModule,
    SharedRoutingModule,    
    MaterialModule
  ],
  providers:[HeaderComponent, FooterComponent],
  declarations: []
})
export class SharedModule { }
