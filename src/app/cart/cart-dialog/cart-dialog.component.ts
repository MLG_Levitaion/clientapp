import { Component, OnInit, Predicate } from '@angular/core';
import { MatDialogRef, MatTableDataSource } from '@angular/material';

import { UserGuard } from 'src/app/shared/guards';
import { RequestOrderModel } from 'src/app/shared/models/order';
import { ReqestOrderItemModel } from 'src/app/shared/models/order-item';
import { OrderService } from 'src/app/shared/services';
import { PaymentService } from 'src/app/shared/services/payment.service';

@Component({
  selector: 'app-cart-dialog',
  templateUrl: './cart-dialog.component.html',
  styleUrls: ['./cart-dialog.component.css'],
  providers: [UserGuard]
})

export class CartDialogComponent implements OnInit {

  order: RequestOrderModel;
  dataSource: MatTableDataSource<ReqestOrderItemModel>;
  totalPrice: number;
  displayedColumns: string[];
  transactionIdPredicate: Predicate<string>;

  constructor(
    public dialogRef: MatDialogRef<CartDialogComponent>,
    private orderService: OrderService,
    private paymentService: PaymentService
    ) { 

    }

  ngOnInit() {
    this.loadOrder();
    this.transactionIdPredicate = this.updateOrder.bind(this);
  }

  loadOrder() {
    this.order = JSON.parse(localStorage.getItem('order'));
    if (!this.order) {
      return;
    }
    
    this.dataSource = new MatTableDataSource(this.order.orderItems);
    this.totalPrice = this.order.orderItems.reduce(function (acc, val) {
      return acc + val.amount;
    }, 0);
  }

  pay() {
    if (this.totalPrice === 0) {
      alert('Your cart is empty');
      return;
    }

    this.orderService.create(this.order).subscribe(data => {
      if (data.errors.length > 0) {
        alert(data.errors.toString());
        return;
      }

      this.order.id = data.id;
      this.paymentService.pay(this.totalPrice, this.transactionIdPredicate);

      localStorage.removeItem('order');
    });
  }

  updateOrder(transactionId: string) {
    this.orderService.update({ orderId: this.order.id, transactionId: transactionId }).subscribe(data => {
      if (data.errors.length > 0) {
        alert(data.errors.toString());
      }
    });

    this.dialogRef.close();
  }

  edit(orderItem: ReqestOrderItemModel) {
    let removedItem = this.order.orderItems.find(item => item.printingEditionId == orderItem.printingEditionId);
    let removedItemIndex = this.order.orderItems.indexOf(removedItem);
    this.order.orderItems.splice(removedItemIndex, 1);
    
    localStorage.setItem('order', JSON.stringify(this.order));

    this.loadOrder();
  }
}