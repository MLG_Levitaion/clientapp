import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserGuard } from '../shared/guards';
import { CartDialogComponent } from './cart-dialog/cart-dialog.component';


export const routes: Routes = [
   { path: '', component: CartDialogComponent, canActivate:[UserGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CartRoutingModule { }
