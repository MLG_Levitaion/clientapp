import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CartRoutingModule } from './cart-routing.module';
import { MaterialModule } from '../shared/material/material.module';
import { CartDialogComponent } from './cart-dialog/cart-dialog.component';


@NgModule({
  declarations: [CartDialogComponent],
  imports: [
    FormsModule,
    MaterialModule,
    CommonModule,
    CartRoutingModule
  ]
})
export class CartModule { }
