import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { OrderRoutingModule } from './order-routing.module';
import { OrdersComponent } from './orders/orders.component';
import { MaterialModule } from '../shared/material/material.module';
import { UserOrdersComponent } from './user-orders/user-orders.component';


@NgModule({
  declarations: [OrdersComponent, UserOrdersComponent],
  imports: [
    FormsModule,
    MaterialModule,
    CommonModule,
    OrderRoutingModule
  ]
})
export class OrderModule { }
