import { Component, OnInit, ViewChild, Predicate } from '@angular/core';
import { MatTableDataSource, PageEvent, MatPaginator, MatSort } from '@angular/material';

import { UserGuard } from 'src/app/shared/guards';
import { FilterOrderModel, ResponseOrderModel } from 'src/app/shared/models/order';
import { OrderService, AccountService } from 'src/app/shared/services';
import { OrderStatus, SortBy, ProductType } from 'src/app/shared/models/enums';
import { ResponseModel } from 'src/app/shared/models/base';
import { Constants } from 'src/app/shared/constants';
import { PaymentService } from 'src/app/shared/services/payment.service';

@Component({
  selector: 'app-user-orders',
  templateUrl: './user-orders.component.html',
  styleUrls: ['./user-orders.component.css'],
  providers: [UserGuard]
})
export class UserOrdersComponent implements OnInit {


  dataSource: MatTableDataSource<ResponseOrderModel>;
  pageEvent: PageEvent;
  count: number;
  searchString: string;
  displayedColumns: string[];
  filterModel: FilterOrderModel;
  transactionIdPredicate: Predicate<string>;
  paidOrderId: number;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    private orderService: OrderService,
    private accountService: AccountService,
    private paymentService: PaymentService,
  ) {
    this.displayedColumns  = ['id', 'creationdate', 'title', 'producttype', 'count', 'amount', 'orderamount', 'status'];

    this.filterModel = new FilterOrderModel();
    this.filterModel.idUser = this.accountService.currentUserValue.id;
    this.filterModel.page = 1;
    this.filterModel.pageSize = 10;
    this.filterModel.status = [];
  }

  ngOnInit() {
    this.loadOrders();
    this.transactionIdPredicate = this.updateOrder.bind(this);
  }

  loadOrders() {
    this.orderService.getFiltered(this.filterModel).subscribe((data: ResponseModel<ResponseOrderModel>) => {
      data.items.map(item => {
        item.status = OrderStatus[item.status];
        item.orderItems.map(orderitem => {
          orderitem.productType = ProductType[orderitem.productType];
        });
      })

      this.dataSource = new MatTableDataSource(data.items);
      this.paginator.length = data.count;
    });
  }

  sortData(event?: MatSort): void {
    this.filterModel.sortField = event.active;

    if (event.direction === Constants.ascending) {
      this.filterModel.sortBy = SortBy.ascending;
    }

    if (event.direction === Constants.descending) {
      this.filterModel.sortBy = SortBy.descending;
    }

    if (event.direction === Constants.emptyString) {
      this.filterModel.sortBy = SortBy.none;
    }
    this.loadOrders();
  }

  updateTable(event?: PageEvent): void {
    this.filterModel.page = event.pageIndex + 1;
    this.filterModel.pageSize = event.pageSize;
    this.loadOrders();
  }

  pay(order: ResponseOrderModel) {
    this.paidOrderId = order.id;
    this.paymentService.pay(order.orderAmount, this.transactionIdPredicate);
  }

  updateOrder(transactionId: string) {
    this.orderService.update({ orderId: this.paidOrderId, transactionId: transactionId }).subscribe(data => {
      if (data.errors.length > 0) {
        alert(data.errors.toString());
        return;
      }
      
      this.paidOrderId = 0;
    }
    );
  }
}
