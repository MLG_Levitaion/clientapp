import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, PageEvent, MatPaginator, MatSort } from '@angular/material';

import { AdminGuard } from 'src/app/shared/guards';
import { FilterOrderModel, ResponseOrderModel } from 'src/app/shared/models/order';
import { OrderService } from 'src/app/shared/services';
import { OrderStatus, SortBy, ProductType } from 'src/app/shared/models/enums';
import { ResponseModel } from 'src/app/shared/models/base';
import { Constants } from 'src/app/shared/constants';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css'],
  providers: [AdminGuard]
})

export class OrdersComponent implements OnInit {

  dataSource: MatTableDataSource<ResponseOrderModel>;
  pageEvent: PageEvent;
  count: number;
  searchString: string;
  displayedColumns: string[];
  filterModel: FilterOrderModel;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    private orderService: OrderService,
  ) {
    this.displayedColumns  = ['id','creationdate','username','useremail','title','producttype','count','amount','orderamount','status'];

    this.filterModel = new FilterOrderModel();
    this.filterModel.page = 1;
    this.filterModel.pageSize = 10;
    this.filterModel.status = [];
  }

  ngOnInit() {
    this.loadOrders();
  }

  loadOrders() {
    this.orderService.getFiltered(this.filterModel).subscribe((data: ResponseModel<ResponseOrderModel>) => {
      data.items.map(item => {
        item.status = OrderStatus[item.status];
        item.orderItems.map(orderitem => {
          orderitem.productType = ProductType[orderitem.productType];
        });
      })
      
      this.dataSource = new MatTableDataSource(data.items);
      this.paginator.length = data.count;
    });
  }

  sortData(event?: MatSort): void {
    this.filterModel.sortField = event.active;

    if (event.direction === Constants.ascending) {
      this.filterModel.sortBy = SortBy.ascending;
    }

    if (event.direction === Constants.descending) {
      this.filterModel.sortBy = SortBy.descending;
    }

    if (event.direction === Constants.emptyString) {
      this.filterModel.sortBy = SortBy.none;
    }
    this.loadOrders();
  }

  updateTable(event?: PageEvent): void {
    this.filterModel.page = event.pageIndex + 1;
    this.filterModel.pageSize = event.pageSize;
    this.loadOrders();
  }
}
