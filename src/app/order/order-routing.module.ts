import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdersComponent } from './orders/orders.component';
import { AdminGuard, UserGuard } from '../shared/guards';
import { UserOrdersComponent } from './user-orders/user-orders.component';


export const routes: Routes = [
  { path: 'orders', component: OrdersComponent, canActivate:[AdminGuard]},
  { path: 'myorders', component: UserOrdersComponent, canActivate:[UserGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule { }
