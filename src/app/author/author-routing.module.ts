import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthorsComponent } from './authors/authors.component';
import { AuthorDialogComponent } from './author-dialog/author-dialog.component';
import { AdminGuard } from '../shared/guards';

export const routes: Routes = [ 
  { path: '', component: AuthorsComponent, canActivate: [AdminGuard] },
  { path: 'edit', component: AuthorDialogComponent, canActivate: [AdminGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthorRoutingModule { }
