import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthorService } from 'src/app/shared/services/author.service';
import { MatTableDataSource, MatPaginator, PageEvent, MatSort } from '@angular/material';
import { MatDialog } from '@angular/material/dialog';

import { ResponseAuthorModelItem, FilterAuthorModel, AuthorModel } from 'src/app/shared/models/authors';
import { SortBy } from 'src/app/shared/models/enums';
import { Constants } from 'src/app/shared/constants/constants';
import { ResponseModel } from 'src/app/shared/models/base';
import { AuthorDialogComponent } from '../author-dialog/author-dialog.component';
import { AdminGuard } from 'src/app/shared/guards';


@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css'],
  providers: [AdminGuard],
})
export class AuthorsComponent implements OnInit {

  dataSource: MatTableDataSource<ResponseAuthorModelItem>;
  pageEvent: PageEvent;
  count: number;
  searchString: string;
  displayedColumns: string[];
  filterModel: FilterAuthorModel;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    private authorService: AuthorService,
    public dialog: MatDialog,
  ) {
    this.displayedColumns = ['name', 'editions', 'edit'];

    this.filterModel = new FilterAuthorModel();
    this.filterModel.page = 1;
    this.filterModel.pageSize = 10;
    this.filterModel.sortBy = SortBy.none;
  }

  ngOnInit(): void {
    this.loadAuthors();
  }

  create(): void {
    const dialogRef = this.dialog.open(AuthorDialogComponent, {
      data: {
        title: 'Create author',
        okButtonText: 'Create'
      }
    });
    dialogRef.afterClosed().subscribe(data =>
      location.reload()
    );
  }

  edit(author?: AuthorModel): void {
    const dialogRef = this.dialog.open(AuthorDialogComponent, {
      data: {
        title: 'Edit author',
        okButtonText: 'Update',
        id: author.id,
        name: author.name
      }
    });
    dialogRef.afterClosed().subscribe(data =>
      location.reload()
    );
  }

  updateTable(event?: PageEvent): void {
    this.filterModel.page = event.pageIndex + 1;
    this.filterModel.pageSize = event.pageSize;
    this.loadAuthors();
  }

  delete(author?: AuthorModel): void {
    this.authorService.delete(author.id).subscribe(data =>
      location.reload()
    );
  }

  sortData(event?: MatSort): void {
    this.filterModel.sortField = event.active;

    if (event.direction === Constants.ascending) {
      this.filterModel.sortBy = SortBy.ascending;
    }

    if (event.direction === Constants.descending) {
      this.filterModel.sortBy = SortBy.descending;
    }

    if (event.direction === Constants.emptyString) {
      this.filterModel.sortBy = SortBy.none;
    }
    this.loadAuthors();
  }

  loadAuthors(): void {
    this.authorService.getFiltered(this.filterModel)
      .subscribe((data: ResponseModel<ResponseAuthorModelItem>) => {
        this.dataSource = new MatTableDataSource<ResponseAuthorModelItem>(data.items);
        this.paginator.length = data.count;
      });
  }
}
