import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { AuthorService } from 'src/app/shared/services';
import { CustomErrorMatcher } from 'src/app/shared/helpers/custom-error-matcher';
import { AuthorDialogModel } from 'src/app/shared/models/authors';
import { AdminGuard } from 'src/app/shared/guards';

@Component({
  selector: 'app-author-dialog',
  templateUrl: './author-dialog.component.html',
  styleUrls: ['./author-dialog.component.css'],
  providers: [AdminGuard],
})
export class AuthorDialogComponent implements OnInit {

  authorForm: FormGroup;
  matcher: CustomErrorMatcher;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<AuthorDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AuthorDialogModel,
    private authorService: AuthorService
  ) {
    this.matcher = new CustomErrorMatcher();
    this.authorForm = this.formBuilder.group({
      name: ['', Validators.required],
    });
  }

  ngOnInit() {

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onConfirmClick(): void {

    if (!this.authorForm.valid) {
      return;
    }

    if (!this.data.id) {
      this.authorService.create(this.data).subscribe();
      this.dialogRef.close();
    }

    this.authorService.update(this.data).subscribe();
    this.dialogRef.close();
  }
}
