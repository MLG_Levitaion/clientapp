import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routes } from './author-routing.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AuthorDialogComponent } from './author-dialog/author-dialog.component';
import { AuthorsComponent } from './authors/authors.component';
import { MaterialModule } from '../shared/material/material.module';
import { AuthorService } from '../shared/services';

@NgModule({
  declarations: [AuthorsComponent, AuthorDialogComponent],
  imports: [
    MaterialModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [AuthorService]
})
export class AuthorModule { }
