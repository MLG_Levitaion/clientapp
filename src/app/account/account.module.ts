import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AccountRoutingModule, routes } from './account-routing.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { EmailconfirmComponent } from './email-confirmed/email-confirmed.component';
import { PasswordrecoveryComponent } from './passwordrecovery/passwordrecovery.component';
import { MaterialModule } from 'src/app/shared/material/material.module'
import { ConfirmEmailDialogComponent } from './confirm-email-dialog/confirm-email-dialog.component';

@NgModule({
  declarations: [LoginComponent, RegisterComponent, EmailconfirmComponent, PasswordrecoveryComponent, ConfirmEmailDialogComponent],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    AccountRoutingModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  exports: [
  ],
  providers: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AccountModule { }
