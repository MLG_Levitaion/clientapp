import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { EmailconfirmComponent } from './email-confirmed/email-confirmed.component';
import { PasswordrecoveryComponent } from './passwordrecovery/passwordrecovery.component';
import { ConfirmEmailDialogComponent } from './confirm-email-dialog/confirm-email-dialog.component';

export const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'emailconfirm', component: EmailconfirmComponent },
  { path: 'passwordrecovery', component: PasswordrecoveryComponent },
  { path: 'confirmemaildialog', component: ConfirmEmailDialogComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
