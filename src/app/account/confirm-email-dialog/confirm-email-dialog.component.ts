import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-confirm-email-dialog',
  templateUrl: './confirm-email-dialog.component.html',
  styleUrls: ['./confirm-email-dialog.component.css']
})
export class ConfirmEmailDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ConfirmEmailDialogComponent>,
  ) { }

  ngOnInit() {
  }

}
