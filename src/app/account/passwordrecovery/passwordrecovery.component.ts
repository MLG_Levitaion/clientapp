import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import { AccountService } from 'src/app/shared/services/account.service';
import { CustomErrorMatcher } from 'src/app/shared/helpers/custom-error-matcher';

@Component({
  selector: 'app-passwordrecovery',
  templateUrl: './passwordrecovery.component.html',
  styleUrls: ['./passwordrecovery.component.css']
})
export class PasswordrecoveryComponent implements OnInit {

  resetForm: FormGroup;
  submitted: boolean;
  email: string;
  matcher: CustomErrorMatcher;

  constructor(
    private formBuilder: FormBuilder,
    private accountSerice: AccountService
  ) {
    this.resetForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
    this.matcher = new CustomErrorMatcher();
  }
  ngOnInit() {

  }

  onResetClick(): void {
    if (this.resetForm.invalid) {
      return;
    }

    this.accountSerice.forgotpassword(this.email)
      .subscribe(data => {
        if(data.errors.length > 0){
          alert(data.errors.toString());
        }
        });
  }
}
