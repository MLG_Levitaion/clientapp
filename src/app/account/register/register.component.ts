import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';

import { AccountService } from 'src/app/shared/services/account.service';
import { CustomErrorMatcher } from 'src/app/shared/helpers/custom-error-matcher';
import { RegisterModel } from 'src/app/shared/models/account/register.model';
import { Constants } from 'src/app/shared/constants/constants'
import { ConfirmEmailDialogComponent } from 'src/app/account/confirm-email-dialog/confirm-email-dialog.component';


@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

    readonly emailPattern = Constants.emailPattern;
    registerForm: FormGroup;
    user: RegisterModel;
    passwordConfirm: string;
    emailConfirm: string;
    matcher: CustomErrorMatcher;

    constructor(
        private formBuilder: FormBuilder,
        private accountSerice: AccountService,
        public dialog: MatDialog,
    ) {
        this.matcher = new CustomErrorMatcher();
        this.user = new RegisterModel();
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            firstname: [Validators.required],
            lastname: [Validators.required],
            username: [Validators.required],
            email: [[Validators.required], Validators.pattern(this.emailPattern)],
            emailconfirm: [],
            password: [[Validators.required, Validators.minLength(6)]],
            passwordconfirm: []
        }, {
            validators: [this.passwordMatchValidator, this.emailMatchValidator]
        }
        );
    }
    passwordMatchValidator(group: FormGroup) {
        var res = group.get('password').value !== group.get('passwordconfirm').value;

        if (!res) {
            group.controls.passwordconfirm.setErrors(null);
            return res;
        }

        group.controls.passwordconfirm.setErrors({ 'mismatch': true });
        return res;
    }

    emailMatchValidator(group: FormGroup) {
        var res = group.get('email').value !== group.get('emailconfirm').value;

        if (!res) {
            group.controls.emailconfirm.setErrors(null);
            return res;
        }

        group.controls.emailconfirm.setErrors({ 'mismatch': true });
        return res;
    }

    register() {
        if (this.registerForm.invalid) {
            return;
        }

        this.accountSerice.register(this.user)
            .subscribe(data => {
                if (data.errors.length > 0) {
                    alert(data.errors.toString());
                    return;
                }
                
                this.dialog.open(ConfirmEmailDialogComponent);
            });
    }
}
