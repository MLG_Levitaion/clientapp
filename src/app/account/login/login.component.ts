import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Constants } from 'src/app/shared/constants';
import { CustomErrorMatcher } from 'src/app/shared/helpers/custom-error-matcher';
import { AccountService } from 'src/app/shared/services';
import { UserModel } from 'src/app/shared/models/users';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})


export class LoginComponent implements OnInit {

    readonly emailPattern = Constants.emailPattern;

    loginForm: FormGroup;
    returnUrl: string;
    email: string;
    password: string;

    matcher: CustomErrorMatcher;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private accountService: AccountService
    ) {
        this.matcher = new CustomErrorMatcher();
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });

        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    ngOnInit() {
    }

    login(): void {
        if (this.loginForm.invalid) {
            return;
        }

        this.accountService.login(this.email, this.password).subscribe((data: UserModel) => {

            if (data.errors.length > 0) {
                alert(data.errors.toString());
                return;
            }

            this.router.navigate([this.returnUrl]);
        });
    }
}
