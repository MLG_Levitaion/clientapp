import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, PageEvent, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { FormControl } from '@angular/forms';

import { PrintingEditionService } from 'src/app/shared/services';
import { SortBy, Currency, ProductType } from 'src/app/shared/models/enums';
import { Constants } from 'src/app/shared/constants';
import { ResponseModel } from 'src/app/shared/models/base';
import { FilterPrintingEditionModel, PrintingEditionModel } from 'src/app/shared/models/printing-editions';
import { EditPrintingEditionDialogComponent } from '../edit-printing-edition-dialog/edit-printing-edition-dialog.component';

@Component({
  selector: 'app-admin-printing-editions',
  templateUrl: './admin-printing-editions.component.html',
  styleUrls: ['./admin-printing-editions.component.css']
})
export class AdminPrintingEditionsComponent implements OnInit {

  dataSource: MatTableDataSource<PrintingEditionModel>;
  pageEvent: PageEvent;
  count: number;
  searchString: string;
  currency: string;

  productTypes: FormControl;
  productTypesList: string[];
  selectedTypes: string;

  displayedColumns: string[];
  filterModel: FilterPrintingEditionModel;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    private printingEditionSerice: PrintingEditionService,
    public dialog: MatDialog
  ) {
    this.productTypes = new FormControl([ProductType[1], ProductType[2], ProductType[3]]);
    this.productTypesList = [ProductType[1], ProductType[2], ProductType[3]];

    this.displayedColumns  = ['title', 'authors', 'productType', 'price', 'edit'];

    this.currency = 'USD';
    this.filterModel = new FilterPrintingEditionModel();
    this.filterModel.page = 1;
    this.filterModel.pageSize = 10;
    this.filterModel.sortBy = SortBy.none;
    this.filterModel.printingEditionTypes = [ProductType['book'], ProductType['magazine'], ProductType['newspaper']];
  }

  ngOnInit(): void {
    this.loadPrintingEditions();
  }

  loadPrintingEditions(): void {
    this.printingEditionSerice.get(this.filterModel)
      .subscribe((data: ResponseModel<PrintingEditionModel>) => {
        data.items.map(item => {
          item.currency = Currency[item.currency]
          item.productType = ProductType[item.productType]
        });

        this.dataSource = new MatTableDataSource<PrintingEditionModel>(data.items);
        this.paginator.length = data.count;
      });
  }

  create(): void {
    const dialogRef = this.dialog.open(EditPrintingEditionDialogComponent, {
      data: {
        dialogTitle: 'Create printing edition',
        okButtonText: 'Create'
      }
    });

    dialogRef.afterClosed().subscribe(data =>
      location.reload()
    );
  }

  setCurrency() {
    this.filterModel.currency = Currency[this.currency];
    this.loadPrintingEditions();
  }

  edit(printingEdition?: PrintingEditionModel): void {
    const dialogRef = this.dialog.open(EditPrintingEditionDialogComponent, {
      data: {
        dialogTitle: 'Edit printing edition',
        okButtonText: 'Update',
        id: printingEdition.id,
        title: printingEdition.title,
        currency: printingEdition.currency,
        description: printingEdition.description,
        authors: printingEdition.authors,
        price: printingEdition.price,
        productType: printingEdition.productType,
      }
    });

    dialogRef.afterClosed().subscribe(data =>
      location.reload()
    );
  }

  updateTable(event?: PageEvent): void {
    this.filterModel.page = event.pageIndex + 1;
    this.filterModel.pageSize = event.pageSize;
    this.loadPrintingEditions();
  }

  delete(printingEdition?: PrintingEditionModel): void {
    this.printingEditionSerice.delete(printingEdition.id).subscribe(data =>
      location.reload()
    );
  }

  setProductTypes(event) {
    this.filterModel.printingEditionTypes = [];
    this.selectedTypes = this.productTypes.value && this.productTypes.value.toString();

    let productTypes = this.selectedTypes.split(',');
    if (productTypes[0] !== "") {
      productTypes.forEach(type => {
        this.filterModel.printingEditionTypes.push(ProductType[type]);
      });
    }

    this.loadPrintingEditions();
  }

  sortData(event?: MatSort): void {
    this.filterModel.sortField = event.active;

    if (event.direction === Constants.ascending) {
      this.filterModel.sortBy = SortBy.ascending;
    }

    if (event.direction === Constants.descending) {
      this.filterModel.sortBy = SortBy.descending;
    }

    if (event.direction === Constants.emptyString) {
      this.filterModel.sortBy = SortBy.none;
    }
    
    this.loadPrintingEditions();
  }

}
