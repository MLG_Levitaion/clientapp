import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PrintingEditionRoutingModule, routes } from './printing-edition-routing.module';
import { MaterialModule } from '../shared/material/material.module';
import { HomePrintingEditionsComponent } from './home-printing-editions/home-printing-editions.component';
import { DetailsPrintingEditionDialogComponent } from './details-printing-edition-dialog/details-printing-edition-dialog.component';
import { AdminPrintingEditionsComponent } from './admin-printing-editions/admin-printing-editions.component';
import { EditPrintingEditionDialogComponent } from './edit-printing-edition-dialog/edit-printing-edition-dialog.component';

@NgModule({
  declarations: [HomePrintingEditionsComponent, DetailsPrintingEditionDialogComponent, AdminPrintingEditionsComponent, EditPrintingEditionDialogComponent],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    CommonModule,
    PrintingEditionRoutingModule,
    RouterModule.forChild(routes)
  ]
})
export class PrintingEditionModule { }