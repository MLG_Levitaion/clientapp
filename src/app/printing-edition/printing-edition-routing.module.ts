import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePrintingEditionsComponent } from './home-printing-editions/home-printing-editions.component';
import { UserGuard, AdminGuard } from '../shared/guards';
import { DetailsPrintingEditionDialogComponent } from './details-printing-edition-dialog/details-printing-edition-dialog.component';
import { EditPrintingEditionDialogComponent } from './edit-printing-edition-dialog/edit-printing-edition-dialog.component';
import { AdminPrintingEditionsComponent } from './admin-printing-editions/admin-printing-editions.component';


export const routes: Routes = [
  { path: '', component: HomePrintingEditionsComponent},
  { path: 'details', component: DetailsPrintingEditionDialogComponent, canActivate:[UserGuard] },
  { path: 'list', component: AdminPrintingEditionsComponent, canActivate:[AdminGuard] },
  { path: 'edit', component: EditPrintingEditionDialogComponent, canActivate:[AdminGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintingEditionRoutingModule { }
