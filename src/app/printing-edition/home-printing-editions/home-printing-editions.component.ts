import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { PageEvent, MatPaginator, MatCheckbox, MatDialog } from '@angular/material';

import { ProductType, SortBy, Currency } from 'src/app/shared/models/enums';
import { PrintingEditionService } from 'src/app/shared/services';
import { FilterPrintingEditionModel, PrintingEditionModel } from 'src/app/shared/models/printing-editions';
import { ResponseModel } from 'src/app/shared/models/base';
import { DetailsPrintingEditionDialogComponent } from '../details-printing-edition-dialog/details-printing-edition-dialog.component';

@Component({
  selector: 'app-home-printing-editions',
  templateUrl: './home-printing-editions.component.html',
  styleUrls: ['./home-printing-editions.component.css']
})

export class HomePrintingEditionsComponent implements OnInit {

  filterForm: FormGroup;
  pageEvent: PageEvent;
  columnsCount: number;
  sidebarWidth: number;
  types: any;
  filterModel: FilterPrintingEditionModel;
  mode: FormControl;
  items: PrintingEditionModel[];
  count: number;
  orderBy: string;
  currency: string;

  productTypes:FormControl;
  productTypesList: string[];
  selectedTypes: string;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  @ViewChild(MatCheckbox, { static: false }) checkBox: MatCheckbox;

  constructor(
    private printingEditionService: PrintingEditionService,
    public dialog: MatDialog,
  ) {
    this.productTypes  = new FormControl([ProductType[1], ProductType[2], ProductType[3]]);
    this.productTypesList  = [ProductType[1], ProductType[2], ProductType[3]];

    this.mode = new FormControl('side');

    this.orderBy = '';
    this.currency = 'USD';

    this.types = [
      { name: ProductType[ProductType.book], checked: false },
      { name: ProductType[ProductType.magazine], checked: false },
      { name: ProductType[ProductType.newspaper], checked: false }
    ];

    this.items = [];
    this.filterModel = new FilterPrintingEditionModel();
    this.filterModel.page = 1;
    this.filterModel.pageSize = 10;
    this.filterModel.minPrice = 0;
    this.filterModel.maxPrice = 10000;
    this.filterModel.printingEditionTypes = [];
  }

  ngOnInit() {
    this.columnsCount = window.innerWidth / 400;
    this.sidebarWidth = window.innerWidth / 400;
    this.loadPrintingEditions();
  }

  loadPrintingEditions() {
    this.printingEditionService.get(this.filterModel).subscribe((data: ResponseModel<PrintingEditionModel>) => {
      data.items.map((item: PrintingEditionModel) => {
        item.productType = ProductType[item.productType];
        item.currency = Currency[item.currency];
      });

      this.items = data.items;
      this.paginator.length = data.count;
    });
  }

  openDetails(item: PrintingEditionModel) {
    this.dialog.open(DetailsPrintingEditionDialogComponent, {
      data: item
    });
  }

  setSortingOrder() {
    this.filterModel.sortField = "id";
    this.filterModel.sortBy = SortBy.ascending;

    if (this.orderBy === "priceasc") {
      this.filterModel.sortField = "price";
      this.filterModel.sortBy = SortBy.ascending;
    }

    if (this.orderBy === "pricedesc") {
      this.filterModel.sortField = "price";
      this.filterModel.sortBy = SortBy.descending;
    }

    if (this.orderBy === "titleasc") {
      this.filterModel.sortField = "title";
      this.filterModel.sortBy = SortBy.ascending;
    }

    if (this.orderBy === "titledesc") {
      this.filterModel.sortField = "title";
      this.filterModel.sortBy = SortBy.descending;
    }

    this.loadPrintingEditions();
  }

  setCurrency() {
    this.filterModel.currency = Currency[this.currency];
    this.loadPrintingEditions();
  }

  updatePages(event?: PageEvent): void {
    this.filterModel.page = event.pageIndex + 1;
    this.filterModel.pageSize = event.pageSize;

    this.loadPrintingEditions();
  }

  onResize() {
    this.columnsCount = window.innerWidth / 400;
  }

  setProductTypes(event) {
    this.filterModel.printingEditionTypes= [];
    this.selectedTypes = this.productTypes.value && this.productTypes.value.toString();

    let productTypes = this.selectedTypes.split(',');
    if (productTypes[0] !== "") {
      productTypes.forEach(type => {
        this.filterModel.printingEditionTypes.push(ProductType[type]);
      });
    }
    
    this.loadPrintingEditions();
  }
}
