import { Component, OnInit, Inject } from '@angular/core';
import { PrintingEditionModel } from 'src/app/shared/models/printing-editions';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { RequestOrderModel } from 'src/app/shared/models/order';
import { AccountService, PrintingEditionService } from 'src/app/shared/services';
import { ReqestOrderItemModel } from 'src/app/shared/models/order-item';
import { Currency } from 'src/app/shared/models/enums';

@Component({
  selector: 'app-details-printing-edition-dialog',
  templateUrl: './details-printing-edition-dialog.component.html',
  styleUrls: ['./details-printing-edition-dialog.component.css']
})

export class DetailsPrintingEditionDialogComponent implements OnInit {

  order: RequestOrderModel;
  count: number;

  constructor(
    public dialogRef: MatDialogRef<DetailsPrintingEditionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: PrintingEditionModel,
    private accountService: AccountService,
    private printingEditionService: PrintingEditionService,
  ) {
    this.order = JSON.parse(localStorage.getItem('order'));
    this.count = 1;
  }

  ngOnInit() {
  }

  onClose() {
    this.dialogRef.close();
  }

  onBuyClick() {
    if (!this.order) {
      this.order = new RequestOrderModel();
      this.order.userId = this.accountService.currentUserValue.id;
      this.order.orderItems = [];

      localStorage.setItem('order', JSON.stringify(this.order));
    }

    let orderItem = this.order.orderItems.find(orderItem => orderItem.printingEditionId == this.data.id)
    if (orderItem) {
      orderItem.count += this.count;
      this.printingEditionService.convert(this.data.price, Currency[this.data.currency]).subscribe(data => {
        orderItem.price = data.price;
        orderItem.amount = orderItem.price * this.count;
        localStorage.setItem('order', JSON.stringify(this.order));
      });

      this.dialogRef.close();
      return;
    }

    orderItem = new ReqestOrderItemModel();
    orderItem.printingEditionId = this.data.id;
    orderItem.title = this.data.title;
    orderItem.count = this.count;

    this.printingEditionService.convert(this.data.price, Currency[this.data.currency]).subscribe(data => {
      orderItem.price = data.price;
      orderItem.amount = orderItem.price * this.count;
      this.order.orderItems.push(orderItem);
      
      localStorage.setItem('order', JSON.stringify(this.order));
    });

    this.dialogRef.close();
  }
}
