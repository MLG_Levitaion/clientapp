import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatSpinner,MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { PrintingEditionDialogModel } from 'src/app/shared/models/printing-editions';
import { PrintingEditionService, AuthorService } from 'src/app/shared/services';
import { CustomErrorMatcher } from 'src/app/shared/helpers/custom-error-matcher';
import { ProductType, Currency } from 'src/app/shared/models/enums';
import { AuthorModel } from 'src/app/shared/models/authors';

@Component({
  selector: 'app-edit-printing-edition-dialog',
  templateUrl: './edit-printing-edition-dialog.component.html',
  styleUrls: ['./edit-printing-edition-dialog.component.css']
})
export class EditPrintingEditionDialogComponent implements OnInit {

  printingEditionForm: FormGroup;
  matcher: CustomErrorMatcher;
  authorsList: AuthorModel[];
  authors: FormControl;
  authorIds: number[];

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditPrintingEditionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: PrintingEditionDialogModel,
    private printingEditionService: PrintingEditionService,
    private authorService: AuthorService,
  ) {
    this.matcher = new CustomErrorMatcher();
    this.printingEditionForm = this.formBuilder.group({
      title: ['', Validators.required],
      description:['',Validators.required],
      price:['',Validators.required]
    });
  }

  ngOnInit() {
    this.authorIds=[];
    this.authorService.getall().subscribe(data=>this.authorsList=data.items);  

    if(!this.data.authors){
      this.data.authors =[];
    }

    this.data.authors.forEach(item=>this.authorIds.push(item.id));
    this.authors = new FormControl(this.authorIds);
  }

  setAuthors(){
    this.data.authors=[];
    let x = this.authors.value;

    this.authors.value.forEach(item => {   
      let x = new AuthorModel();
      x.id = item;
    this.data.authors.push(x);
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onConfirmClick(): void {
    this.data.productType = ProductType[this.data.productType];
    this.data.currency = Currency[this.data.currency];
    
    if (!this.printingEditionForm.valid) {
      return;
    }

    if (!this.data.id) {
      this.printingEditionService.create(this.data).subscribe();
      this.dialogRef.close();
    }

    this.printingEditionService.update(this.data).subscribe();
    this.dialogRef.close();
  }

}
