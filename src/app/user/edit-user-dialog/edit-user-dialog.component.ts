import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { UserDialogModel } from 'src/app/shared/models/users';
import { UserService } from 'src/app/shared/services';
import { CustomErrorMatcher } from 'src/app/shared/helpers/custom-error-matcher';
import { AdminGuard } from 'src/app/shared/guards';

@Component({
  selector: 'app-edit-user-dialog',
  templateUrl: './edit-user-dialog.component.html',
  styleUrls: ['./edit-user-dialog.component.css'],
  providers: [AdminGuard]
})

export class EditUserDialogComponent implements OnInit {

  userForm: FormGroup;
  matcher: CustomErrorMatcher;

  constructor(private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserDialogModel,
    private userService: UserService
  ) {
    this.matcher = new CustomErrorMatcher();

    this.userForm = this.formBuilder.group({
      username: ['', Validators.required],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      isblocked:[]
    });
  }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onConfirmClick(): void {
    if (!this.userForm.valid) {
      return;
    }
    
    this.userService.update(this.data).subscribe();
    this.dialogRef.close();
  }
}
