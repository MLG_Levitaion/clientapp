import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileComponent } from './profile/profile.component';
import { AuthGuard, AdminGuard } from '../shared/guards';
import { UsersComponent } from './users/users.component';
import { EditUserDialogComponent } from './edit-user-dialog/edit-user-dialog.component';


export const routes: Routes = [
  { path: 'profile', component: ProfileComponent, canActivate:[AuthGuard] },
  { path: 'users', component: UsersComponent, canActivate:[AdminGuard] },
  { path: 'edit', component: EditUserDialogComponent, canActivate:[AdminGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
