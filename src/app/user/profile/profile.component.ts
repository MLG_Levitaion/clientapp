import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UserService } from 'src/app/shared/services/user.service';
import { UserModel } from 'src/app/shared/models/users/user.model';
import { AccountService } from 'src/app/shared/services';
import { CustomErrorMatcher } from 'src/app/shared/helpers/custom-error-matcher';
import { ChangePasswordModel } from 'src/app/shared/models/account';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [UserService],
})

export class ProfileComponent implements OnInit {

  profileForm: FormGroup;

  user: UserModel;
  changePasswordModel: ChangePasswordModel;  
  confirmOldPassword: string;
  matcher: CustomErrorMatcher;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private accountService: AccountService,
  ) {
    this.matcher = new CustomErrorMatcher();
    this.user = new UserModel();
    this.changePasswordModel = new ChangePasswordModel();

    this.profileForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      userName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      oldPassword: ['', Validators.minLength(6)],
      newPassword: ['', Validators.minLength(6)],
      confirmNewPassword: [],
    },
      {
        validators: [this.passwordMatchValidator]
      });

    this.profileForm.disable();
  }
  passwordMatchValidator(group: FormGroup) {
    var res = group.get('newPassword').value !== group.get('confirmNewPassword').value;

    if (!res) {
      group.controls.confirmNewPassword.setErrors(null);
      return res;
    }

    group.controls.confirmNewPassword.setErrors({ 'mismatch': true });
    return res;
  }

  ngOnInit() {
    this.accountService.currentUser.subscribe(x => this.user = x);
  }

  handleInputChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }

  _handleReaderLoaded(e) {
    let reader = e.target;
    this.user.profilePicture = reader.result;
    console.log(this.user.profilePicture)
  }

  edit() {
    this.changePasswordModel.id = this.accountService.currentUserValue.id;
    if (!this.profileForm.valid) {
      return;
    }
    
    if (this.changePasswordModel.newPassword) {
      this.accountService.changePassword(this.changePasswordModel).subscribe(data=>{
        if(data.errors.length > 0){
          alert(data.errors.toString());
          return;
        }
        alert('Your password changed successfuly')
      });
    }

    this.userService.update(this.user).subscribe(data => {
      if (data.errors.length > 0) {
        alert(data.errors.toString());
      }
      this.accountService.setUserValue(this.user);
    });

    this.profileForm.disable();
  }
  
  cancel() {
    this.user = this.accountService.currentUserValue;
    this.profileForm.disable();
  }


}
