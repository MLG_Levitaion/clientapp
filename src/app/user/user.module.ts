import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ProfileComponent } from './profile/profile.component';
import { routes } from './user-routing.module';
import { MaterialModule } from '../shared/material/material.module';
import { UsersComponent } from './users/users.component';
import { EditUserDialogComponent } from './edit-user-dialog/edit-user-dialog.component';


@NgModule({
  declarations: [ProfileComponent, UsersComponent, EditUserDialogComponent],
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    MaterialModule,
    RouterModule.forChild(routes)
  ]
})
export class UserModule { }
