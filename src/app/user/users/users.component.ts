import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, PageEvent, MatSort } from '@angular/material';
import { MatDialog } from '@angular/material/dialog';

import { SortBy } from 'src/app/shared/models/enums';
import { Constants } from 'src/app/shared/constants/constants';
import { ResponseModel } from 'src/app/shared/models/base';
import { AdminGuard } from 'src/app/shared/guards';
import { EditUserDialogComponent } from '../edit-user-dialog/edit-user-dialog.component';
import { UserService } from 'src/app/shared/services';
import { FilterUserModel, UserModel } from 'src/app/shared/models/users';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [AdminGuard],
})
export class UsersComponent implements OnInit {

  dataSource: MatTableDataSource<UserModel>;
  pageEvent: PageEvent;
  count: number;
  searchString: string;
  displayedColumns: string[];
  filterModel: FilterUserModel;
  isBlocked: boolean;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    private userService: UserService,
    public dialog: MatDialog
  ) {
    this.displayedColumns  = ['username','email','name','block','edit'];
    
    this.filterModel = new FilterUserModel();
    this.filterModel.page = 1;
    this.filterModel.pageSize = 10;
    this.filterModel.sortBy = SortBy.none;
  }

  ngOnInit(): void {
    this.loadUsers();
  }

  loadUsers(): void {
    this.userService.getFiltered(this.filterModel)
      .subscribe((data: ResponseModel<UserModel>) => {
        this.dataSource = new MatTableDataSource<UserModel>(data.items);
        this.paginator.length = data.count;
      });
  }

  delete(user?: UserModel): void {
    this.userService.delete(user.id).subscribe();
  }

  edit(user?: UserModel): void {
    const dialogRef = this.dialog.open(EditUserDialogComponent, {
      data: {
        title: 'Edit user',
        okButtonText: 'Update',
        id: user.id,
        userName: user.userName,
        lastName: user.lastName,
        firstName: user.firstName,
        email: user.email,
        isBlocked: user.isBlocked
      }
    });

    dialogRef.afterClosed().subscribe(data =>
      location.reload()
    );
  }

  updateTable(event?: PageEvent): void {
    this.filterModel.page = event.pageIndex + 1;
    this.filterModel.pageSize = event.pageSize;
    this.loadUsers();
  }

  sortData(event?: MatSort): void {
    this.filterModel.sortField = event.active;

    if (event.direction === Constants.ascending) {
      this.filterModel.sortBy = SortBy.ascending;
    }

    if (event.direction === Constants.descending) {
      this.filterModel.sortBy = SortBy.descending;
    }

    if (event.direction === Constants.emptyString) {
      this.filterModel.sortBy = SortBy.none;
    }
    
    this.loadUsers();
  }
}
